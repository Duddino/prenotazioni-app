import 'package:flutter/material.dart';
import 'calendar.dart';
import 'login.dart';
import 'session.dart';
import 'user_data.dart';
import 'register.dart';
import 'change_user_password.dart';

class HomeWidget extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<HomeWidget> {
  final laboratories = [
    "Informatica 1",
    "Informatica 2",
    "Multimediale 1",
    "Multimediale 2",
    "Biologia",
    "Chimica",
    "Fisica",
    "Linguistico"
  ];
  final icons = [
    Icons.laptop,
    Icons.laptop,
    Icons.tv,
    Icons.tv,
    Icons.nature,
    Icons.science,
    Icons.backpack,
    Icons.language
  ];
  var index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Laboratorio ${laboratories[index]}"),
        ),
        body: CalendarWidget(laboratories[index], index, key: ValueKey(index)),
        drawer: Drawer(
            child: ListView(padding: EdgeInsets.zero, children: <Widget>[
          Image(image: AssetImage("assets/logo.png")),
          Container(
              child: Center(
                  child: Text("Laboratori",
                      style: TextStyle(fontWeight: FontWeight.bold)))),
          for (var i = 0; i < laboratories.length; i++)
            ListTile(
                leading: Icon(icons[i]),
                title: Text(laboratories[i]),
                onTap: () {
                  setState(() {
                    index = i;
                  });
                  Navigator.pop(context);
                }),
          if (UserData.isAdmin)
            Container(
                child: Center(
                    child: Text("Admin",
                        style: TextStyle(fontWeight: FontWeight.bold)))),
          if (UserData.isAdmin)
            ListTile(
                leading: Icon(Icons.add),
                title: Text("Aggiungi un docente"),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => RegisterWidget()));
                }),
          if (UserData.isAdmin)
            ListTile(
                leading: Icon(Icons.add),
                title: Text("Cambia la password di un utente"),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ChangeUserPasswordWidget()));
                }),
          Container(
              child: Center(
                  child: Text("Account",
                      style: TextStyle(fontWeight: FontWeight.bold)))),
          ListTile(
              leading: Icon(Icons.logout),
              title: Text("Logout"),
              onTap: () {
                Session.logout();
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => LoginWidget()));
              }),
        ])));
  }
}
