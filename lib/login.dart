import 'package:flutter/material.dart';
import 'session.dart';
import 'home.dart';

class LoginWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text("Prenotazioni Laboratori")),
      ),
      body: LoginFormWidget(),
    );
  }
}

class LoginFormWidget extends StatefulWidget {
  LoginFormWidget({Key key}) : super(key: key);

  @override
  LoginFormState createState() => LoginFormState();
}

class LoginFormState extends State<LoginFormWidget> {
  final _formKey = GlobalKey<FormState>();
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  var loading = false;
  var rememberPassword = true;

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: ListView(padding: EdgeInsets.all(40), children: <Widget>[
          Container(
              child: Image(image: AssetImage("assets/logo.png"), height: 200)),
          SizedBox(height: 10),
          TextFormField(
              controller: usernameController,
              decoration: const InputDecoration(
                  hintText: "Inserisci lo username",
                  labelText: "Username",
                  border: OutlineInputBorder()),
              validator: (value) {
                if (value.isEmpty) {
                  return "Inserisci il tuo username";
                }
                return null;
              }),
          SizedBox(height: 10),
          TextFormField(
              controller: passwordController,
              decoration: const InputDecoration(
                  hintText: "Inserisci la password",
                  labelText: 'Password',
                  border: OutlineInputBorder()),
              obscureText: true,
              validator: (value) {
                if (value.isEmpty) {
                  return "Inserisci la password";
                }
                return null;
              }),
          SizedBox(height: 10),
          Row(
            children: [
              Expanded(
                child: FlatButton(
                    color: Colors.blue,
                    textColor: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    onPressed: () async {
                      if (!loading && _formKey.currentState.validate()) {
                        loading = true;
                        var username = usernameController.text;
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: Text("Caricamento"),
                        ));
                        var response = await Session.login(username,
                            passwordController.text, rememberPassword);
                        Scaffold.of(context).removeCurrentSnackBar();
                        loading = false;
                        if (response) {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HomeWidget()));
                        } else {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: Text("Dati errati"),
                          ));
                        }
                      }
                    },
                    child: Text("Login")),
              ),
            ],
          ),
          Row(
            children: [
              Checkbox(
                value: rememberPassword,
                onChanged: (value) {
                  setState(() {
                    rememberPassword = value;
                  });
                },
              ),
              Text("Ricorda la password"),
            ],
          ),
        ]));
  }
}
