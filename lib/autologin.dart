import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'home.dart';
import 'login.dart';
import 'session.dart';

class AutoLogin extends StatefulWidget {
  @override
  _AutoLoginState createState() => _AutoLoginState();
}

class _AutoLoginState extends State<AutoLogin> {
  final secureStorage = FlutterSecureStorage();

  Future<bool> future;
  bool isDataAvailable = false;

  @override
  void initState() {
    super.initState();
    future = Session.isLoggedIn();
  }

  Future<bool> autoLogin() async {
    String username = await secureStorage.read(key: "username");
    String password = await secureStorage.read(key: "password");
    return Session.login(username, password);
  }

  Future<void> tryToLogin() async {
    var isLoggedIn = await Session.isLoggedIn();
    if (isLoggedIn) {
      var success = await autoLogin();
      if (success) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => HomeWidget()));
        return;
      }
    }

    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => LoginWidget()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Prenotazioni")),
      body: FutureBuilder(
          future: tryToLogin(),
          builder: (context, snapshot) {
            return Center(
              child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.green)),
            );
          }),
    );
  }
}
