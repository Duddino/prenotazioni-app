import 'dart:io';

import 'package:flutter/material.dart';
import 'session.dart';
import 'dart:convert';

class ChangeUserPasswordWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Cambia la password")),
        body: ChangeUserPasswordFormWidget());
  }
}

class ChangeUserPasswordFormWidget extends StatefulWidget {
  @override
  State<ChangeUserPasswordFormWidget> createState() {
    return ChangeUserPasswordFormState();
  }
}

class ChangeUserPasswordFormState extends State<ChangeUserPasswordFormWidget> {
  final _formKey = GlobalKey<FormState>();
  final _passwordController = TextEditingController();
  String _name = "admin";
  bool loading = false;
  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(children: <Widget>[
          DropdownButton(
            value: _name,
            items: List.of(Session.getUsers().map((user) {
              return DropdownMenuItem(
                  child: Text(usernameToName(user)), value: user);
            })),
            onChanged: (String value) {
              setState(() {
                _name = value;
              });
            },
          ),
          TextFormField(
              controller: _passwordController,
              decoration:
                  const InputDecoration(hintText: "Inserisci la password"),
              obscureText: true,
              validator: (value) {
                if (value.isEmpty) {
                  return "Inserisci la password";
                }
                return null;
              }),
          RaisedButton(
              onPressed: () async {
                await onFormSubmit();
              },
              child: Text("Cambia password"))
        ]));
  }

  Future<void> onFormSubmit() async {
    if (!loading && _formKey.currentState.validate()) {
      loading = true;
      var password = _passwordController.text;
      Scaffold.of(context).showSnackBar(SnackBar(content: Text("Caricamento")));
      var response = await changePassword(_name, password);
      Scaffold.of(context).removeCurrentSnackBar();
      loading = false;
      if (response) {
        Scaffold.of(context).showSnackBar(
            SnackBar(content: Text("Password cambiata con successo!")));
      } else {
        Scaffold.of(context).showSnackBar(
            SnackBar(content: Text("C'è stato un errore con la connessione!")));
      }
    }
  }

  Future<bool> changePassword(String name, String password) async {
    var response = json.decode(await Session.post(
        "${Session.serverIp}/cambia_password",
        jsonEncode({"name": name, "password": password})));
    return response["ok"];
  }

  String usernameToName(String username) {
    // TODO: remove duplicate code
    var names = username.split(".");
    assert(names.length == 2 || names.length == 1);
    if (names.length < 2) {
      return username;
    } else {
      return "${names[0][0].toUpperCase()}${names[0].substring(1)} ${names[1][0].toUpperCase()}${names[1].substring(1)}"; // ugly
    }
  }
}
