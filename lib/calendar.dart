import 'dart:core';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'session.dart';
import 'user_data.dart';

typedef VoidCallback = void Function();

class CalendarPage extends StatelessWidget {
  final String title;
  final int index;
  CalendarPage(this.title, this.index);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Prenota lab. $title"),
      ),
      body: CalendarWidget(title, index),
    );
  }
}

class CalendarWidget extends StatefulWidget {
  final String title;
  final int index;

  CalendarWidget(this.title, this.index, {Key key}) : super(key: key);

  @override
  CalendarState createState() => CalendarState(title, index);
}

class CalendarState extends State<CalendarWidget> {
  final String title;
  final int index;
  bool loading = false;
  int notAvailableDays = 4; // number of days + the current one
  final week = [
    "Lunedì",
    "Martedì",
    "Mercoledì",
    "Giovedì",
    "Venerdì",
    "Sabato",
    "Domenica",
  ];
  final months = [
    "Gennaio",
    "Febbario",
    "Marzo",
    "Aprile",
    "Maggio",
    "Giugno",
    "Luglio",
    "Agosto",
    "Settembre",
    "Ottobre",
    "Novembre",
    "Dicembre",
  ];
  final freeColor = Colors.green;
  final occupiedColor = Colors.red;
  final reservedColor = Colors.blue;
  final notAvailableColor = Colors.orange;

  Future<List<Prenotazione>> future;
  final currentDay =
      DateTime.now().millisecondsSinceEpoch ~/ (1000 * 60 * 60 * 24);

  CalendarState(this.title, this.index);

  @override
  void initState() {
    future = getPrenotazioni();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: future,
        builder:
            (BuildContext context, AsyncSnapshot<List<Prenotazione>> snapshot) {
          return GridView.builder(
            itemBuilder: (context, i) {
              var offset = (i ~/ 6);
              var day = DateTime.now().add(Duration(days: offset));

              if (i % 6 == 0)
                return Center(
                    child: Text(
                        "${week[day.weekday - 1]}\n${day.day} ${months[day.month - 1]}",
                        style: TextStyle(fontWeight: FontWeight.bold)));
              else if (snapshot.hasData) {
                var reservation = snapshot.data
                    .firstWhere((r) => r.index == i, orElse: () => null);
                var disabled = isReservationDisabled(day, snapshot.data, i);
                if (reservation?.name == "Non disponibile" &&
                    !UserData.isAdmin) {
                  reservation = null;
                  disabled = true;
                }
                if (reservation == null) {
                  return _reservationTile(
                      "libero",
                      disabled ? notAvailableColor : freeColor,
                      disabled
                          ? showNotAvailableDialog
                          : () => onButtonPressedPrenota(i));
                } else if (reservation.name == UserData.username ||
                    UserData.isAdmin) {
                  return _reservationTile(reservation.name, reservedColor,
                      () => showDescriptionDialog(i, reservation));
                } else {
                  return _reservationTile(
                      reservation.name, occupiedColor, () {});
                }
              } else {
                return FlatButton(child: Text("caricamento"));
              }
            },
            gridDelegate:
                SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 6),
            itemCount: 14 * 6, // 2 weeks
            scrollDirection: Axis.horizontal,
          );
        });
  }

  FlatButton _reservationTile(
      String text, Color color, VoidCallback onPressed) {
    return FlatButton(
        child: Text(usernameToName(text)),
        textColor: color,
        splashColor: color,
        disabledTextColor: color,
        onPressed: onPressed);
  }

  bool isReservationDisabled(
      DateTime day, List<Prenotazione> reservations, int i) {
    var filtered =
        reservations.where((var r) => r.index - 1 == i || r.index + 1 == i);
    for (var reservation in filtered) {
      if (reservation?.name != UserData.username && !UserData.isAdmin) {
        return true;
      }
    }
    return !UserData.isAdmin &&
        (day.weekday == 7 ||
            DateTime.now().add(Duration(days: notAvailableDays)).isAfter(day));
  }

  String usernameToName(String username) {
    var names = username.split(".");
    assert(names.length == 2 || names.length == 1);
    if (names.length < 2) {
      return username;
    } else {
      return "${names[0][0].toUpperCase()}${names[0].substring(1)} ${names[1][0].toUpperCase()}${names[1].substring(1)}"; // ugly
    }
  }

  void showDescriptionDialog(int i, Prenotazione prenotazione) {
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
                title: Text("Prenotazione"),
                content: Text(prenotazione.description == ""
                    ? "Nessuna descrizione"
                    : "Descrizione: ${prenotazione.description}"),
                actions: [
                  FlatButton(
                      child: Text("Disdici"),
                      onPressed: () async {
                        await onButtonPressedDisdici(i);
                        Navigator.pop(context);
                      }),
                  FlatButton(
                    child: Text("Ok"),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ]));
  }

  Future<void> onButtonPressedPrenota(int i) async {
    if (!loading) {
      var offset = (i ~/ 6);
      setState(() {
        loading = true;
      });
      if (UserData.isAdmin) {
        var controller = TextEditingController();
        var checkBoxValue = false;
        var name = "admin";
        var description = "";
        var valid = await showDialog(
            context: context,
            builder: (BuildContext context) => StatefulBuilder(
                  builder: (context, setState) => AlertDialog(
                      title: Text("Scegli il docente"),
                      content: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          DropdownButton(
                            value: name,
                            items: List.of(Session.getUsers().map((user) {
                              return DropdownMenuItem(
                                  child: Text(usernameToName(user)),
                                  value: user);
                            })),
                            onChanged: (value) {
                              name = value;
                              setState(() {});
                            },
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                  child:
                                      Text("Segnala ora come non disponibile")),
                              Checkbox(
                                  value: checkBoxValue,
                                  onChanged: (value) {
                                    setState(() {
                                      checkBoxValue = value;
                                      controller.text = "";
                                    });
                                  }),
                            ],
                          ),
                          Expanded(
                            child: TextFormField(
                              controller: controller,
                              decoration: const InputDecoration(
                                  hintText: "Inserisci la descrizione"),
                              maxLines: null,
                              expands: true,
                            ),
                          ),
                        ],
                      ),
                      actions: [
                        FlatButton(
                            child: Text("Ok"),
                            onPressed: () {
                              if (checkBoxValue) {
                                name = "Non disponibile";
                                description = controller.text;
                              }
                              Navigator.pop(context, true);
                            })
                      ]),
                ));
        if (valid != null) {
          var response = await prenotaAdmin(
              currentDay + offset, (i % 6) - 1 + 8, name, description);
          if (!response) {
            Scaffold.of(context).showSnackBar(SnackBar(
                content: Text("Non esiste nessuna classe con quel nome")));
          }
        }
      } else {
        var controller = TextEditingController();
        var description = await showDialog(
            context: context,
            builder: (BuildContext context) => StatefulBuilder(
                  builder: (context, setState) => AlertDialog(
                      title: Text("Inserisci il tipo di esperienza"),
                      content: Column(children: <Widget>[
                        Expanded(
                          child: TextFormField(
                              controller: controller,
                              decoration: const InputDecoration(
                                  hintText: "Inserisci la descrizione"),
                              maxLines: null,
                              keyboardType: TextInputType.multiline,
                              expands: true),
                        ),
                      ]),
                      actions: [
                        FlatButton(
                            child: Text("Cancella"),
                            onPressed: () {
                              Navigator.pop(context, false);
                            }),
                        FlatButton(
                            child: Text("Ok"),
                            onPressed: () {
                              Navigator.pop(context, controller.text);
                            }),
                      ]),
                ));
        if (description is String) {
          await prenota(currentDay + offset, (i % 6) - 1 + 8, description);
        }
      }
      future = getPrenotazioni();
      setState(() {
        loading = false;
      });
    }
  }

  Future<void> onButtonPressedDisdici(int i) async {
    if (!loading) {
      var offset = (i ~/ 6);
      setState(() {
        loading = true;
      });
      await disdici(currentDay + offset, (i % 6) - 1 + 8);
      future = getPrenotazioni();
      setState(() {
        loading = false;
      });
    }
  }

  Future<List<Prenotazione>> getPrenotazioni() async {
    var response = await Session.get(
        "${Session.serverIp}/orario?min_h=8&max_h=12&min_day=$currentDay&max_day=${currentDay + 14}&laboratory=$index");
    dynamic thing = json
        .decode(response)
        .map((reservation) => Prenotazione(
            (reservation["day"] - currentDay) * 6 + reservation["hour"] - 7,
            reservation["reserved_to"],
            reservation["description"]))
        .toList();
    return List<Prenotazione>.from(thing);
  }

  Future<void> prenota(int day, int hour, String description) async {
    await Session.post(
        "${Session.serverIp}/prenota",
        jsonEncode({
          "day": day,
          "hour": hour,
          "laboratory": index,
          "description": description
        }));
  }

  Future<bool> prenotaAdmin(
      int day, int hour, String name, String description) async {
    var response = await Session.post(
        "${Session.serverIp}/admin_prenota",
        jsonEncode({
          "day": day,
          "hour": hour,
          "laboratory": index,
          "reserved_to": name,
          "description": description
        }));
    print(response);
    dynamic decodedResponse = json.decode(response);
    return decodedResponse['ok'];
  }

  Future<void> disdici(int day, int hour) async {
    await Session.post("${Session.serverIp}/disdici",
        '{"day": $day, "hour": $hour, "laboratory": $index}');
  }

  void showNotAvailableDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
          title: Text("Ora non disponibile"),
          content: Text(
              "Per prenotare quest'ora è necessario parlare con il tecnico di questo laboratorio"),
          actions: [
            FlatButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.pop(context);
                })
          ]),
    );
  }
}

class Prenotazione {
  int index;
  String name;
  String description;
  Prenotazione(this.index, this.name, this.description);
}
