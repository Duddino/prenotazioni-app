import 'package:flutter/material.dart';
import 'session.dart';
import 'dart:convert';

class RegisterWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Registra un Docente")),
        body: RegisterFormWidget());
  }
}

class RegisterFormWidget extends StatefulWidget {
  @override
  State<RegisterFormWidget> createState() {
    return RegisterFormState();
  }
}

class RegisterFormState extends State<RegisterFormWidget> {
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _surnameController = TextEditingController();
  final _passwordController = TextEditingController();
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(children: <Widget>[
          TextFormField(
              controller: _nameController,
              decoration: const InputDecoration(hintText: "Inserisci il nome"),
              validator: (value) {
                if (value.isEmpty) {
                  return "Inserisci il nome";
                }
                return null;
              }),
          TextFormField(
              controller: _surnameController,
              decoration:
                  const InputDecoration(hintText: "Inserisci il cognome"),
              validator: (value) {
                if (value.isEmpty) {
                  return "Inserisci il cognome";
                }
                return null;
              }),
          TextFormField(
              controller: _passwordController,
              decoration:
                  const InputDecoration(hintText: "Inserisci la password"),
              obscureText: true,
              validator: (value) {
                if (value.isEmpty) {
                  return "Inserisci la password";
                }
                return null;
              }),
          RaisedButton(
              onPressed: () async {
                await onRegisterPressed();
              },
              child: Text("Registra")),
        ]));
  }

  Future<void> onRegisterPressed() async {
    if (!loading && _formKey.currentState.validate()) {
      loading = true;
      var name = _nameController.text.toLowerCase();
      var surname = _surnameController.text.toLowerCase();
      var password = _passwordController.text;
      Scaffold.of(context).showSnackBar(SnackBar(content: Text("Caricamento")));
      var response = await register("$surname.$name", password);
      Scaffold.of(context).removeCurrentSnackBar();
      loading = false;
      if (response) {
        _formKey.currentState.reset();
        Scaffold.of(context).showSnackBar(
            SnackBar(content: Text("Registrazione avvenuta con successo")));
      } else {
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text("Quel utente esiste già!")));
      }
    }
  }

  Future<bool> register(String name, String password) async {
    var response = json.decode(await Session.post(
        "${Session.serverIp}/registra",
        '{"name": "$name", "password": "$password"}'));
    return response["ok"];
  }
}
