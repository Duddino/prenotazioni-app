import 'package:http/http.dart' as http;
import 'dart:core';
import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'user_data.dart';

class Session {
  static Map<String, String> headers = {"content-type": "application/json"};
  static String serverIp = "https://prenotazioni.ddns.net:7777";
  static List<String> _users = [];
  static final secureStorage = FlutterSecureStorage();

  static Future<void> fetchUsers() async {
    _users = List<String>.from(json.decode(await get("$serverIp/users")));
  }

  static Future<String> get(String url) async {
    http.Response response = await http.get(url, headers: headers);
    updateCookie(response);
    return response.body;
  }

  static Future<String> post(String url, dynamic data) async {
    http.Response response = await http.post(url, body: data, headers: headers);
    updateCookie(response);
    print(response.statusCode);
    return response.body;
  }

  static void updateCookie(http.Response response) {
    String rawCookie = response.headers['set-cookie'];
    if (rawCookie != null) {
      int index = rawCookie.indexOf(';');
      headers['cookie'] =
          (index == -1) ? rawCookie : rawCookie.substring(0, index);
    }
  }

  static List<String> getUsers() {
    return _users;
  }

  static saveCredentials(String username, String password) async {
    await secureStorage.write(key: "username", value: username);
    await secureStorage.write(key: "password", value: password);
  }

  static Future<bool> isLoggedIn() async {
    return await secureStorage.containsKey(key: "username");
  }

  static Future<bool> login(String username, String password,
      [bool rememberPassword = true]) async {
    print("Loggin in...");
    var response = json.decode(await Session.post('${Session.serverIp}/login',
        jsonEncode({"username": username, "password": password})));
    if (response["ok"]) {
      fetchUsers();
      if (rememberPassword) {
        saveCredentials(username, password);
      }
      UserData.username = username;
      UserData.isAdmin = response["is_admin"];
    }
    print('${response["ok"]} ${response["is_admin"]}');
    return response["ok"];
  }

  static Future<void> logout() async {
    await secureStorage.deleteAll();
  }
}
